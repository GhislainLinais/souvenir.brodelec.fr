import Vue from 'vue'
import {
  ValidationObserver,
  ValidationProvider,
  extend,
  localize,
  configure
} from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import fr from 'vee-validate/dist/locale/fr.json'

for (var rule in rules) {
  extend(rule, rules[rule])
}

localize('fr', fr)

const config = {
  classes: {
    valid: 'is-valid',
    invalid: 'is-invalid'
  }
}
configure(config)

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)