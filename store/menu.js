import Menu from '@/data/menu'

// state
export const state = () => ({
  data: Menu.data
})

// getters
export const getters = {
}

// mutations
export const mutations = {
}

// actions
export const actions = {
}