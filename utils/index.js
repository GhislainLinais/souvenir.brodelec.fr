const pageTitle = ' | Brodelec Store'

export const serializeTitle = title => {
  return title + pageTitle
}

export const debounce = function(fn, threshold) {
  let timeout
  threshold = threshold || 100
  return function debounced() {
    clearTimeout(timeout)
    let args = arguments
    let _this = this
    function delayed() {
      fn.apply(_this, args)
    }
    timeout = setTimeout(delayed, threshold)
  }
}

export const concatenationUrl = function (url, endpoint, qsParams) {
  let queryString = ''
  let first = true
  for (let qsIndex in qsParams) {
    let qsParam = qsParams[qsIndex]
    if (null !== qsParam) {
      queryString += first ? '' : '&'
      queryString += qsIndex + '=' + qsParam
      first = false
    }
  }

  return `${url[0]}${endpoint}${url[1]}${queryString}`;
}