import { bdcRepository } from './Repository'
import { concatenationUrl } from '@/utils/index'

const resource = 'users'
export default {
  requestAccess(requestData) {
    const endpoint = `${resource}/access-request`

    return bdcRepository.post(concatenationUrl`/${endpoint}`, requestData, {
      transformRequest: [(data) => JSON.stringify(data)],
    })
  }
}