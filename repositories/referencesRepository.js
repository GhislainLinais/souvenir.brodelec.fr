import { bdcRepository } from './Repository'
import { concatenationUrl } from '@/utils/index'

const resource = 'references'
export default {
  getReferencesCollections() {
    const endpoint = `${resource}/public-collections`

    return bdcRepository.get(concatenationUrl`/${endpoint}`)
  }
}