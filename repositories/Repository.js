import axios from 'axios'

const bdcUrl = process.env.NUXT_ENV_BDC_URL
const bdcToken = 'da5d8d4e-242a-49eb-8050-b0d05571f304'
const token = null
const bdcHeaders = {
  'BDC-API-TOKEN': bdcToken,
}
if (token) bdcHeaders['X-Custom-Auth'] = token
export const bdcRepository = axios.create({ 
  baseURL: bdcUrl, 
  headers: bdcHeaders 
})