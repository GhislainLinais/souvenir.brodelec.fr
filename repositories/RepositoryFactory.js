import ReferencesRepository from './referencesRepository'
import UsersRepository from './usersRepository'

const repositories = {
  references: ReferencesRepository,
  users: UsersRepository,
}

export const RepositoryFactory = {
  get: name => repositories[name]
}